<#
.SYNOPSIS
    Functions to calculate the time it takes to run a script
.DESCRIPTION
    This script contains functions that will calculate the time it takes to run a given script in hours, minutes, seconds and milseconds
.PARAMETER Path
.PARAMETER LiteralPath
.Example
    .".\Functions\ScriptTimer.ps1"
    $ScriptTimerStartTime = Start-ScriptTimer
    Get-Process | Format-Table -AutoSize
    Stop-ScriptTimer $ScriptTimerStartTime
#>

function Start-ScriptTimer
{
    $ScriptTimerStartTime = $(get-date)
    return $ScriptTimerStartTime
}

function Format-TimeSpan
{
    PARAM (
        [Parameter(Mandatory = $true, ValueFromPipeline = $true)]
        [TimeSpan]$TimeSpan
    )

    #Current implementation doesn't handle days.

    #By including the delimiters in the formatting string it's easier when we contatenate in the end
    $hours = $TimeSpan.Hours.ToString("00")
    $minutes = $TimeSpan.Minutes.ToString("\00")
    $seconds = $TimeSpan.Seconds.ToString("\00")
    $milliseconds = $TimeSpan.Milliseconds.ToString("\000")

    Write-Output ($hours + " hours " + $minutes + " minutes " + $seconds + " seconds " + $milliseconds + " milliseconds")
}

function Stop-ScriptTimer ([DateTime] $ScriptTimerStartTime)
{
    $ScriptTimerElapsedTime = $(get-date) - $ScriptTimerStartTime
    $ScriptTimerTotalTime = Format-TimeSpan $ScriptTimerElapsedTime.Ticks
    Write-Output "Script completed in $ScriptTimerTotalTime" | Out-String
}

<#
$ScriptTimerStartTime = Start-ScriptTimer
Get-Process | Format-Table -AutoSize
Stop-ScriptTimer $ScriptTimerStartTime
#>